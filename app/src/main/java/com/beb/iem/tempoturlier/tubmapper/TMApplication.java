package com.beb.iem.tempoturlier.tubmapper;

import android.app.Application;

import com.beb.iem.tempoturlier.tubmapper.data.entity.mapper.LineDataMapper;
import com.beb.iem.tempoturlier.tubmapper.data.entity.mapper.StopDataMapper;
import com.beb.iem.tempoturlier.tubmapper.data.entity.mapper.StopGroupDataMapper;
import com.beb.iem.tempoturlier.tubmapper.data.manager.ApiManager;
import com.beb.iem.tempoturlier.tubmapper.data.manager.ApiManagerImpl;
import com.beb.iem.tempoturlier.tubmapper.data.manager.CacheManager;
import com.beb.iem.tempoturlier.tubmapper.data.manager.CacheManagerImpl;
import com.beb.iem.tempoturlier.tubmapper.data.manager.DownloadManager;
import com.beb.iem.tempoturlier.tubmapper.data.manager.DownloadManagerImpl;
import com.beb.iem.tempoturlier.tubmapper.data.repository.DataRepository;

/**
 * Created by romaintempo on 04/11/2016.
 */

public class TMApplication extends Application {
    private static TMApplication app;

    private DataRepository dataRepository;

    public TMApplication() {
        app = this;
        injectDependencies();
    }

    public static TMApplication app() {
        return app;
    }

    public DataRepository getDataRepository() {
        return dataRepository;
    }

    private void injectDependencies() {
        ApiManager apiManager = new ApiManagerImpl();
        DownloadManager downloadManager = new DownloadManagerImpl();
        CacheManager cacheManager = new CacheManagerImpl();

        LineDataMapper lineDataMapper = new LineDataMapper();
        StopDataMapper stopDataMapper = new StopDataMapper();
        StopGroupDataMapper stopGroupDataMapper = new StopGroupDataMapper();

        dataRepository = new DataRepository(apiManager, downloadManager, cacheManager, lineDataMapper, stopDataMapper, stopGroupDataMapper);
    }
}
