package com.beb.iem.tempoturlier.tubmapper.data.entity;

import com.google.gson.annotations.SerializedName;

public class StopEntity {
    @SerializedName("id")
    private String id;
    @SerializedName("label")
    private String label;
    @SerializedName("latitude")
    private float latitude;
    @SerializedName("longitude")
    private float longitude;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String name) {
        this.label = name;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
