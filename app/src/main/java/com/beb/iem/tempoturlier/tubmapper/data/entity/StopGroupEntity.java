package com.beb.iem.tempoturlier.tubmapper.data.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by romaintempo on 04/11/2016.
 */

public class StopGroupEntity {
    @SerializedName("id")
    private String id;
    @SerializedName("line_id")
    private String lineId;
    @SerializedName("way")
    private String way;
    @SerializedName("stop_id")
    private String stopId;
    @SerializedName("order")
    private String order;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getWay() {
        return way;
    }

    public void setWay(String way) {
        this.way = way;
    }

    public String getStopId() {
        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
