package com.beb.iem.tempoturlier.tubmapper.data.entity.mapper;

import com.beb.iem.tempoturlier.tubmapper.data.entity.LineEntity;
import com.beb.iem.tempoturlier.tubmapper.data.model.LineModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by romaintempo on 04/11/2016.
 */

public class LineDataMapper {

    public LineModel transform(LineEntity lineEntity) {
        LineModel lineModel = new LineModel();
        lineModel.setId(lineEntity.getId());
        lineModel.setLabel(lineEntity.getLabel());
        lineModel.setNumber(lineEntity.getNumber());
        lineModel.setColor(lineEntity.getColor());
        return lineModel;
    }

    public List<LineModel> transform(List<LineEntity> lineEntityList) {
        List<LineModel> lineModelList = new ArrayList<>();
        for (LineEntity lineEntity : lineEntityList) {
            lineModelList.add(transform(lineEntity));
        }
        return lineModelList;
    }
}
