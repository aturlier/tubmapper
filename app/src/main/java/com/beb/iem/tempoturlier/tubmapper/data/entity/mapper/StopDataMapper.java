package com.beb.iem.tempoturlier.tubmapper.data.entity.mapper;

import com.beb.iem.tempoturlier.tubmapper.data.entity.StopEntity;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by romaintempo on 04/11/2016.
 */

public class StopDataMapper {

    public StopModel transform(StopEntity stopEntity) {
        StopModel stopModel = new StopModel();
        stopModel.setId(stopEntity.getId());
        stopModel.setLabel(stopEntity.getLabel());
        stopModel.setLatitude(stopEntity.getLatitude());
        stopModel.setLongitude(stopEntity.getLongitude());
        return stopModel;
    }

    public List<StopModel> transform(List<StopEntity> stopEntityList) {
        List<StopModel> stopModelList = new ArrayList<>();
        for(StopEntity stopEntity : stopEntityList) {
            stopModelList.add(transform(stopEntity));
        }
        return stopModelList;
    }
    
}
