package com.beb.iem.tempoturlier.tubmapper.data.entity.mapper;

import com.beb.iem.tempoturlier.tubmapper.data.entity.StopGroupEntity;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopGroupModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by romaintempo on 04/11/2016.
 */

public class StopGroupDataMapper {

    public StopGroupModel transform(StopGroupEntity stopGroupEntity) {
        StopGroupModel stopGroupModel = new StopGroupModel();
        stopGroupModel.setId(stopGroupEntity.getId());
        stopGroupModel.setWay(stopGroupEntity.getWay());
        stopGroupModel.setOrder(stopGroupEntity.getOrder());
        stopGroupModel.setLineId(stopGroupEntity.getLineId());
        stopGroupModel.setStopId(stopGroupEntity.getStopId());
        return stopGroupModel;
    }

    public List<StopGroupModel> transform(List<StopGroupEntity> stopGroupEntityList) {
        List<StopGroupModel> stopGroupModelList = new ArrayList<>();
        for(StopGroupEntity stopGroupEntity : stopGroupEntityList) {
            stopGroupModelList.add(transform(stopGroupEntity));
        }
        return stopGroupModelList;
    }

}
