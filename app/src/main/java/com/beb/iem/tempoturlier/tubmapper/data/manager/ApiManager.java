package com.beb.iem.tempoturlier.tubmapper.data.manager;

import com.beb.iem.tempoturlier.tubmapper.data.entity.LineEntity;
import com.beb.iem.tempoturlier.tubmapper.data.entity.StopEntity;
import com.beb.iem.tempoturlier.tubmapper.data.entity.StopGroupEntity;

import java.util.List;

import rx.Observable;

/**
 * Created by romaintempo on 19/10/2016.
 */

public interface ApiManager {
    String API_BASE_URL = "https://tub.bourgmapper.fr/api/";

    Observable<List<LineEntity>> getAllLines();
    Observable<LineEntity> getLineById(String id);
    Observable<List<StopEntity>> getAllStops();
    Observable<StopEntity> getStopById(String id);
    Observable<List<StopEntity>> getAllStopByPathId(String id);
    Observable<List<StopGroupEntity>> getAllStopGroups();
    Observable<StopGroupEntity> getStopGroupById(String id);
    Observable<List<StopGroupEntity>> getAllStopGroupsByPathId(String id);
}
