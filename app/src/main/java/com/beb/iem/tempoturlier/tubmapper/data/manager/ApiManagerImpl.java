package com.beb.iem.tempoturlier.tubmapper.data.manager;

import com.beb.iem.tempoturlier.tubmapper.data.entity.LineEntity;
import com.beb.iem.tempoturlier.tubmapper.data.entity.StopEntity;
import com.beb.iem.tempoturlier.tubmapper.data.entity.StopGroupEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by romaintempo on 19/10/2016.
 */

public class ApiManagerImpl implements ApiManager {

    private ApiService apiService;

    private interface ApiService {
        @GET("lines")
        Observable<LineListEnveloppe> getAllLines();

        @GET("lines")
        Observable<LineEnveloppe> getPathById(@Query("id") String id);

        @GET("stops")
        Observable<StopListEnveloppe> getAllStops();

        @GET("stop")
        Observable<StopEnveloppe> getStopById(@Query("id") String id);

        @GET("stops")
        Observable<StopListEnveloppe> getAllStopByPathId(@Query("id") String id);

        @GET("stopgroups")
        Observable<StopGroupListEnveloppe> getAllStopGroups();

        @GET("stopgroups")
        Observable<StopGroupEnveloppe> getStopGroupById(@Query("id") String id);

        @GET("stopgroups")
        Observable<StopGroupListEnveloppe> getAllStopGroupsByLineId(@Query("id") String id);
    }

    public ApiManagerImpl() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    @Override
    public Observable<List<LineEntity>> getAllLines() {
        return apiService.getAllLines().map(new Func1<LineListEnveloppe, List<LineEntity>>() {
            @Override
            public List<LineEntity> call(LineListEnveloppe lineListEnveloppe) {
                return lineListEnveloppe.lines;
            }
        });
    }

    @Override
    public Observable<LineEntity> getLineById(String id) {
        return apiService.getPathById(id).map(new Func1<LineEnveloppe, LineEntity>() {
            @Override
            public LineEntity call(LineEnveloppe lineEnveloppe) {
                return lineEnveloppe.line;
            }
        });
    }

    @Override
    public Observable<List<StopEntity>> getAllStops() {
        return apiService.getAllStops().map(new Func1<StopListEnveloppe, List<StopEntity>>() {
            @Override
            public List<StopEntity> call(StopListEnveloppe stopListEnveloppe) {
                return stopListEnveloppe.stops;
            }
        });
    }

    @Override
    public Observable<StopEntity> getStopById(String id) {
        return apiService.getStopById(id).map(new Func1<StopEnveloppe, StopEntity>() {
            @Override
            public StopEntity call(StopEnveloppe stopEnveloppe) {
                return stopEnveloppe.stop;
            }
        });
    }

    @Override
    public Observable<List<StopEntity>> getAllStopByPathId(String id) {
        return apiService.getAllStopByPathId(id).map(new Func1<StopListEnveloppe, List<StopEntity>>() {
            @Override
            public List<StopEntity> call(StopListEnveloppe stopListEnveloppe) {
                return stopListEnveloppe.stops;
            }
        });
    }

    @Override
    public Observable<List<StopGroupEntity>> getAllStopGroups() {
        return apiService.getAllStopGroups().map(new Func1<StopGroupListEnveloppe, List<StopGroupEntity>>() {
            @Override
            public List<StopGroupEntity> call(StopGroupListEnveloppe stopGroupListEnveloppe) {
                return stopGroupListEnveloppe.stopgroups;
            }
        });
    }

    @Override
    public Observable<StopGroupEntity> getStopGroupById(String id) {
        return apiService.getStopGroupById(id).map(new Func1<StopGroupEnveloppe, StopGroupEntity>() {
            @Override
            public StopGroupEntity call(StopGroupEnveloppe stopGroupEnveloppe) {
                return stopGroupEnveloppe.stopgroup;
            }
        });
    }

    @Override
    public Observable<List<StopGroupEntity>> getAllStopGroupsByPathId(String id) {
        return apiService.getAllStopGroupsByLineId(id).map(new Func1<StopGroupListEnveloppe, List<StopGroupEntity>>() {
            @Override
            public List<StopGroupEntity> call(StopGroupListEnveloppe stopGroupListEnveloppe) {
                return stopGroupListEnveloppe.stopgroups;
            }
        });
    }


    private class LineListEnveloppe {
        List<LineEntity> lines;
    }

    private class LineEnveloppe {
        LineEntity line;
    }

    private class StopEnveloppe {
        StopEntity stop;
    }

    private class StopListEnveloppe {
        List<StopEntity> stops;
    }

    private class StopGroupEnveloppe {
        StopGroupEntity stopgroup;
    }

    private class StopGroupListEnveloppe {
        List<StopGroupEntity> stopgroups;
    }

}
