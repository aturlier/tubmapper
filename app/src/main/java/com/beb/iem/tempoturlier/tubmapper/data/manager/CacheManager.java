package com.beb.iem.tempoturlier.tubmapper.data.manager;

import com.beb.iem.tempoturlier.tubmapper.data.model.LineModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopGroupModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;

import java.util.List;

/**
 * Created by romaintempo on 04/11/2016.
 */

public interface CacheManager {
    List<LineModel> getLineList();
    void setLines(List<LineModel> lineModelList);
    List<StopModel> getStopList();
    void setStops(List<StopModel> stopModelList);
    List<StopGroupModel> getStopGroupList();
    void setStopGroups(List<StopGroupModel> stopGroupModelList);
    StopModel getStopById(String id);
    LineModel getLineById(String id);
}
