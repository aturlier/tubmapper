package com.beb.iem.tempoturlier.tubmapper.data.manager;

import com.beb.iem.tempoturlier.tubmapper.data.model.LineModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopGroupModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aturlier on 21/10/2016.
 */

public class CacheManagerImpl implements CacheManager {
    private List<LineModel> lineList;
    private List<StopModel> stopList;
    private List<StopGroupModel> stopGroupList;

    public CacheManagerImpl(){
        stopList = new ArrayList<>();
    }

    @Override
    public void setLines(List<LineModel> lineModelList) {
        this.lineList = lineModelList;
    }

    @Override
    public void setStops(List<StopModel> stopModelList) {
        this.stopList = stopModelList;
    }

    @Override
    public List<StopGroupModel> getStopGroupList() {
        return stopGroupList;
    }

    @Override
    public void setStopGroups(List<StopGroupModel> stopGroupModelList) {
        this.stopGroupList = stopGroupModelList;
    }

    public List<LineModel> getLineList() {
        return lineList;
    }

    public List<StopModel> getStopList() {
        return stopList;
    }

    public StopModel getStopById(String id) {
        for(StopModel stopModel : stopList) {
            if(stopModel.getId().equals(id)) {
                return stopModel;
            }
        }
        return null;
    }

    public LineModel getLineById(String id) {
        for(LineModel lineModel : lineList) {
            if(lineModel.getId().equals(id)) {
                return lineModel;
            }
        }
        return null;
    }

    /*private ArrayList<LineModel> findPathsWithStop(StopModel stop){
        ArrayList<LineModel> pathsContainingStop = new ArrayList<>();
        for(LineModel line : pathList){
            if(stopList.contains(stop)) {
                pathsContainingStop.add(line);
            }
        }
        return pathsContainingStop;
    }

    public HashMap<LineModel, StopModel> getRoute(StopModel start, StopModel end){
        HashMap<LineModel, StopModel> routeMap = new HashMap<>();
        ArrayList<LineModel> possiblePathsStart = findPathsWithStop(start);
        ArrayList<LineModel> possiblePathsEnd = findPathsWithStop(end);
        ArrayList<LineModel> directPaths = (ArrayList<LineModel>)arrayListComparator(possiblePathsStart, possiblePathsEnd);
        if(directPaths != null && directPaths.size() > 0){
            //Cleans the paths going in the wrong direction.
            for(int i = 0; i < directPaths.size(); i++){
                if(directPaths.get(i).getStopList().indexOf(start) > directPaths.get(i).getStopList().indexOf(end)){
                    directPaths.remove(directPaths.get(i));
                    i--;
                }
            }
            //For now, let's take the first  line in the array. Later, we should put a bit more work in that to get the shortest line
            for(int i = directPaths.get(0).getStopList().indexOf(start); i <= directPaths.get(0).getStopList().indexOf(end); i++){
                routeMap.put(directPaths.get(0), directPaths.get(0).getStopList().get(i));
            }
            return routeMap;
        }
        //create the complex routemap with multiple paths
        return routeMap;
    }

    private static ArrayList<?> arrayListComparator(ArrayList<?> list1, ArrayList<?> list2){
        ArrayList<Object> sameObjects = new ArrayList<>();
        for(Object object : list1){
            for(Object object2 : list2){
                if(object.equals(object2)){
                    sameObjects.add(object);
                }
            }
        }
        return sameObjects;
    }*/
}
