package com.beb.iem.tempoturlier.tubmapper.data.manager;

import okhttp3.ResponseBody;
import rx.Observable;

/**
 * Created by romaintempo on 29/11/2016.
 */

public interface DownloadManager {
    String API_BASE_URL = "https://tub.bourgmapper.fr/download/";

    Observable<ResponseBody> getKmlById(String id);
}
