package com.beb.iem.tempoturlier.tubmapper.data.manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by romaintempo on 29/11/2016.
 */

public class DownloadManagerImpl implements DownloadManager {

    private DownloadService downloadService;

    private interface DownloadService {
        @GET("kml/{id}")
        Observable<ResponseBody> getKmlById(@Path("id") String id);
    }

    public DownloadManagerImpl() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        downloadService = retrofit.create(DownloadService.class);
    }

    @Override
    public Observable<ResponseBody> getKmlById(String id) {
        return downloadService.getKmlById(id);
    }
}
