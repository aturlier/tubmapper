package com.beb.iem.tempoturlier.tubmapper.data.model;

/**
 * Created by romaintempo on 04/11/2016.
 */

public class StopGroupModel {
    private String id;
    private String lineId;
    private String way;
    private String stopId;
    private String order;

    private StopModel stop;
    private LineModel line;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getWay() {
        return way;
    }

    public void setWay(String way) {
        this.way = way;
    }

    public String getStopId() {
        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public StopModel getStop() {
        return stop;
    }

    public void setStop(StopModel stop) {
        this.stop = stop;
    }

    public LineModel getLine() {
        return line;
    }

    public void setLine(LineModel line) {
        this.line = line;
    }
}
