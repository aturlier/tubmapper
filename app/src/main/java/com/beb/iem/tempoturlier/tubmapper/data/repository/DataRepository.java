package com.beb.iem.tempoturlier.tubmapper.data.repository;

import com.beb.iem.tempoturlier.tubmapper.data.entity.LineEntity;
import com.beb.iem.tempoturlier.tubmapper.data.entity.StopEntity;
import com.beb.iem.tempoturlier.tubmapper.data.entity.StopGroupEntity;
import com.beb.iem.tempoturlier.tubmapper.data.entity.mapper.LineDataMapper;
import com.beb.iem.tempoturlier.tubmapper.data.entity.mapper.StopDataMapper;
import com.beb.iem.tempoturlier.tubmapper.data.entity.mapper.StopGroupDataMapper;
import com.beb.iem.tempoturlier.tubmapper.data.manager.ApiManager;
import com.beb.iem.tempoturlier.tubmapper.data.manager.CacheManager;
import com.beb.iem.tempoturlier.tubmapper.data.manager.DownloadManager;
import com.beb.iem.tempoturlier.tubmapper.data.model.LineModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopGroupModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;
import com.beb.iem.tempoturlier.tubmapper.data.utils.ArrayUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by romaintempo on 04/11/2016.
 */

public class DataRepository {

    private ApiManager apiManager;
    private DownloadManager downloadManager;
    private CacheManager cacheManager;

    private LineDataMapper lineDataMapper;
    private StopDataMapper stopDataMapper;
    private StopGroupDataMapper stopGroupDataMapper;

    public DataRepository(ApiManager apiManager, DownloadManager downloadManager, CacheManager cacheManager, LineDataMapper lineDataMapper, StopDataMapper stopDataMapper, StopGroupDataMapper stopGroupDataMapper) {
        this.apiManager = apiManager;
        this.downloadManager = downloadManager;
        this.cacheManager = cacheManager;
        this.lineDataMapper = lineDataMapper;
        this.stopDataMapper = stopDataMapper;
        this.stopGroupDataMapper = stopGroupDataMapper;
    }

    public Observable<List<LineModel>> getAllLinesCall(){
        return apiManager.getAllLines().map(new Func1<List<LineEntity>, List<LineModel>>() {
            @Override
            public List<LineModel> call(List<LineEntity> lineEntityList) {
                List<LineModel> lineModelList = lineDataMapper.transform(lineEntityList);
                cacheManager.setLines(lineModelList);
                return lineModelList;
            }
        });
    }

    public Observable<Boolean> getAllStopsAndStopGroupsCall(){
        return apiManager.getAllLines().flatMap(new Func1<List<LineEntity>, Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call(List<LineEntity> lineEntityList) {
                List<LineModel> lineModelList = lineDataMapper.transform(lineEntityList);
                cacheManager.setLines(lineModelList);
                return apiManager.getAllStops().flatMap(new Func1<List<StopEntity>, Observable<Boolean>>() {
                    @Override
                    public Observable<Boolean> call(List<StopEntity> stopEntities) {

                        List<StopModel> stopModelList = stopDataMapper.transform(stopEntities);
                        cacheManager.setStops(stopModelList);

                        return apiManager.getAllStopGroups().flatMap(new Func1<List<StopGroupEntity>, Observable<Boolean>>() {
                            @Override
                            public Observable<Boolean> call(List<StopGroupEntity> stopGroupEntities) {

                                List<StopGroupModel> stopGroupModelList = stopGroupDataMapper.transform(stopGroupEntities);
                                for (int i = 0; i < stopGroupEntities.size(); i++) {
                                    stopGroupModelList.get(i).setLine(cacheManager.getLineById(stopGroupModelList.get(i).getLineId()));
                                    stopGroupModelList.get(i).setStop(cacheManager.getStopById(stopGroupModelList.get(i).getStopId()));
                                }
                                cacheManager.setStopGroups(stopGroupModelList);
                                return Observable.just(true);
                            }
                        });
                    }
                });
            }
        });

    }

    public Observable<InputStream> getKmlById(String id) {
        return downloadManager.getKmlById(id).map(new Func1<ResponseBody, InputStream>() {
            @Override
            public InputStream call(ResponseBody responseBody) {
                return responseBody.byteStream();
            }
        });

    }

    public List<StopModel> getAllStops() {
        return cacheManager.getStopList();
    }

    public List<LineModel> getAllLines() {
        return cacheManager.getLineList();
    }

    public List<StopModel> getStopListForLine(final LineModel line){
        if(cacheManager.getStopGroupList() != null) {
            ArrayList<StopModel> stopsContainedInLine = new ArrayList<>();
            List<StopGroupModel> stopGroupModels = cacheManager.getStopGroupList();
            for (StopGroupModel stopGroupModel : stopGroupModels) {
                if (stopGroupModel.getLine().getId().equals(line.getId())) {
                    stopsContainedInLine.add(stopGroupModel.getStop());
                }
            }
            return ArrayUtils.cleanArray(stopsContainedInLine);
        }
        return null;
    }

    public List<LineModel> getLineListForStop(StopModel stop){
        ArrayList<LineModel> linesContainingStop = new ArrayList<>();
        List<StopGroupModel> stopGroupModels = cacheManager.getStopGroupList();
        for(StopGroupModel stopGroupModel : stopGroupModels){
            if(stopGroupModel.getStop().getId().equals(stop.getId())){
                linesContainingStop.add(stopGroupModel.getLine());
            }
        }
        return linesContainingStop;
    }

    public LineModel getLineForId(String id) {
        return cacheManager.getLineById(id);
    }

}
