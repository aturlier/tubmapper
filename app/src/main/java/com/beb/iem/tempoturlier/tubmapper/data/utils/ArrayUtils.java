package com.beb.iem.tempoturlier.tubmapper.data.utils;

import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aturlier on 29/11/2016.
 */

public class ArrayUtils {

    public static boolean isStopAlreadyInArray(List<StopModel> stopList, StopModel stop) {
        for (StopModel stopInArray : stopList) {
            if (stopInArray.getLabel().equals(stop.getLabel())) {
               return true;
            }
        }
        return false;
    }

    public static List<StopModel> cleanArray(List<StopModel> stopList) {
        ArrayList<StopModel> cleanArray = new ArrayList<>();
        cleanArray.addAll(stopList);
        for (int i = 0; i < cleanArray.size() - 1; i++) {
            for (int j = i + 1; j < cleanArray.size(); j++) {
                if (cleanArray.get(i).getLabel().equals(cleanArray.get(j).getLabel())) {
                    cleanArray.remove(j);
                }
            }
        }
        stopList.clear();
        return cleanArray;
    }

}
