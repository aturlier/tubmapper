package com.beb.iem.tempoturlier.tubmapper.ui.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.beb.iem.tempoturlier.tubmapper.R;
import com.beb.iem.tempoturlier.tubmapper.ui.custom.ScreenSlidePagerAdapter;
import com.beb.iem.tempoturlier.tubmapper.ui.fragment.LineListFragment;
import com.beb.iem.tempoturlier.tubmapper.ui.fragment.MapFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.beb.iem.tempoturlier.tubmapper.ui.fragment.MapFragment.PERMISSIONS_REQUEST_FINE_LOCATION;

public class MainActivity extends AppCompatActivity {

    private ScreenSlidePagerAdapter pagerAdapter;

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.main_pager_tab_layout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        checkPermissions();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSIONS_REQUEST_FINE_LOCATION) {
            setupViewPager();
        }
    }

    public MapFragment getMapFragment() {
        return (MapFragment) pagerAdapter.getItem(0);
    }

    private void checkPermissions() {
        if (!(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) ||
                !(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST_FINE_LOCATION);
        }
        else {
            setupViewPager();
        }
    }

    private void setupViewPager() {
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(MapFragment.newInstance(), getString(R.string.pager_tab_title_map));
        pagerAdapter.addFragment(LineListFragment.newInstance(), getString(R.string.pager_tab_title_list));
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
