package com.beb.iem.tempoturlier.tubmapper.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewConfiguration;

import com.beb.iem.tempoturlier.tubmapper.R;


public class SplashScreen extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        int flags = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && !ViewConfiguration.get(this).hasPermanentMenuKey()) {
            flags += View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            flags += View.SYSTEM_UI_FLAG_FULLSCREEN
                    + View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    + View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            if (!ViewConfiguration.get(this).hasPermanentMenuKey()) {
                flags += View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            flags += View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && flags > 0) {
            getWindow().getDecorView().getRootView().setSystemUiVisibility(flags);
        }

        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(mainActivity);
                finish();
            }
        }, 1800);
    }
}
