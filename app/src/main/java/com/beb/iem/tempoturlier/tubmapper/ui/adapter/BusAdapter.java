package com.beb.iem.tempoturlier.tubmapper.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.beb.iem.tempoturlier.tubmapper.R;
import com.beb.iem.tempoturlier.tubmapper.data.model.LineModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;
import com.beb.iem.tempoturlier.tubmapper.ui.fragment.MapFragment;
import com.beb.iem.tempoturlier.tubmapper.ui.listener.BusCellView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;

/**
 * Created by aturlier on 02/11/2016.
 */

public class BusAdapter extends RecyclerView.Adapter<BusAdapter.BusViewHolder> {

    private final int adapterLayoutId = R.layout.bus_adapter_cell;
    private LayoutInflater inflater;
    private Context context;
    private List<LineModel> lines;
    Map<LineModel, List<StopModel>> linesAndStops;
    private List<BusCellView> busCells;
    private MapFragment mapFragment;

    public BusAdapter(Context context, MapFragment mapFragment) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.mapFragment = mapFragment;
        linesAndStops = new TreeMap<>();
        busCells = new ArrayList<>();
    }

    public void setList(HashMap<LineModel, List<StopModel>> linesAndStops) {
        this.linesAndStops = linesAndStops;
        this.lines = new ArrayList<>(this.linesAndStops.keySet());
        Collections.sort(lines, new Comparator<LineModel>() {
            @Override
            public int compare(LineModel o1, LineModel o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        notifyDataSetChanged();
    }

    @Override
    public BusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BusViewHolder(inflater.inflate(adapterLayoutId, parent, false));
    }

    @Override
    public void onBindViewHolder(BusViewHolder holder, int position) {
        holder.setLineNumberText(lines.get(position).getNumber());
        holder.setLineId(getLineId(position));
        String name = lines.get(position).getLabel().split(" : ")[1];
        String startName = name.split(" <> ")[0];
        String endName = name.split(" <> ")[1];
        holder.setLineStartText(startName);
        holder.setLineEndText(endName);
        holder.setCardViewColor(Color.parseColor(lines.get(position).getColor()));
        holder.setExpendableStopList((ArrayList<StopModel>) linesAndStops.get(lines.get(position)));
        if (!busCells.contains(holder)) {
            busCells.add(holder);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getLineId(int position) {
        return lines.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return linesAndStops.size();
    }

    class BusViewHolder extends RecyclerView.ViewHolder implements BusCellView {

        @BindView(R.id.line_row_card_view) CardView cardView;
        @BindView(R.id.numberTextView) TextView lineNumber;
        @BindView(R.id.lineStartTextView) TextView lineStart;
        @BindView(R.id.lineEndTextView) TextView lineEnd;
        @BindView(R.id.bus_adapter_cell_stop_recyclerview) RecyclerView stopListLayout;
        @BindView(R.id.bus_adapter_cell_stop_list_container_framelayout) FrameLayout stopListContainer;

        private boolean expendableLayoutState = false;
        private String lineId;

        public String getLineId() {
            return lineId;
        }

        public void setLineId(String lineId) {
            this.lineId = lineId;
        }

        public BusViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setCardViewColor(int color) {
            cardView.setCardBackgroundColor(color);
        }

        public void setLineNumberText(String lineNumber) {
            this.lineNumber.setText(lineNumber);
        }

        public void setLineStartText(String lineStart) {
            this.lineStart.setText(lineStart);
        }

        public void setLineEndText(String lineEnd) {
            this.lineEnd.setText(lineEnd);
        }

        public void setExpendableStopList(ArrayList<StopModel> stopList){
            stopListLayout.setAdapter(new StopAdapter(stopList, BusAdapter.this.mapFragment, lineId));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            stopListLayout.setLayoutManager(linearLayoutManager);
            TransitionManager.beginDelayedTransition(stopListContainer);
            stopListContainer.setVisibility(View.GONE);
        }

        @OnClick(R.id.cardViewContainer)
        public void cardViewClicked() {
            Log.i(TAG, "cardClicked: ");

            BusAdapter.this.busCells.remove(this);

            for (BusCellView busCellView : busCells) {
                busCellView.collapse();
            }

            if (expendableLayoutState) {
                TransitionManager.beginDelayedTransition(stopListContainer);
                stopListContainer.setVisibility(View.GONE);
                expendableLayoutState = false;
            } else {
                TransitionManager.beginDelayedTransition(stopListContainer);
                stopListContainer.setVisibility(View.VISIBLE);
                expendableLayoutState = true;
            }

            busCells.add(this);
        }

        @Override
        public void collapse() {
            TransitionManager.beginDelayedTransition(stopListContainer);
            stopListContainer.setVisibility(View.GONE);
            expendableLayoutState = false;
        }
    }
}
