package com.beb.iem.tempoturlier.tubmapper.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.beb.iem.tempoturlier.tubmapper.R;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;
import com.beb.iem.tempoturlier.tubmapper.ui.fragment.MapFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by aturlier on 28/11/2016.
 */

public class StopAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<StopModel> stopList;
    private String lineId;
    private MapFragment mapFragment;

    public StopAdapter(List<StopModel> stopList, MapFragment mapFragment, String lineId) {
        this.stopList = stopList;
        this.mapFragment = mapFragment;
        this.lineId = lineId;
    }

    public final int STOP_ITEM_TYPE_VALUE = 10;
    public final int OPTIONS_ITEM_TYPE_VALUE = 20;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder holder;
        if(viewType == STOP_ITEM_TYPE_VALUE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stop_list_cell, parent, false);
            holder = new StopViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stop_list_option_cell, parent, false);
            holder = new OptionsViewHolder(view);
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder toCastHolder, int position) {
        if(toCastHolder instanceof StopViewHolder) {
            StopViewHolder holder = (StopViewHolder) toCastHolder;
            if (position == 0) {
                holder.stopIndicator.setImageResource(R.drawable.first_stop);
            } else if (position == stopList.size() - 1) {
                holder.stopIndicator.setImageResource(R.drawable.last_stop);
            } else {
                holder.stopIndicator.setImageResource(R.drawable.common_stops);
            }

            holder.stopName.setText(stopList.get(position).getLabel());
        } else if (toCastHolder instanceof OptionsViewHolder) {
            OptionsViewHolder holder = (OptionsViewHolder) toCastHolder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return OPTIONS_ITEM_TYPE_VALUE;
        } else {
            return STOP_ITEM_TYPE_VALUE;
        }
    }

    @Override
    public int getItemCount() {
        return stopList.size() + 1;
    }

    static class StopViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.stop_list_cell_textView)
        TextView stopName;

        @BindView(R.id.stop_list_cell_image)
        ImageView stopIndicator;

        public StopViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


    class OptionsViewHolder extends RecyclerView.ViewHolder {

        private boolean highlighted = false;

        public OptionsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.highlight_line_stop_list_option_cell_button)
        public void onHighlightButtonClick() {
            if (highlighted) {
                ((Button)this.itemView.findViewById(R.id.highlight_line_stop_list_option_cell_button)).setText("Show on map");
                StopAdapter.this.mapFragment.resetHighlighting();
            } else {
                ((Button)this.itemView.findViewById(R.id.highlight_line_stop_list_option_cell_button)).setText("Reset Map Highlighting");
                StopAdapter.this.mapFragment.highlightLayerOnMap(StopAdapter.this.lineId);
            }
            highlighted = !highlighted;
        }

        @OnClick(R.id.directions_stop_list_option_cell_button)
        public void onDirectionsButtonClick() {

        }
    }
}
