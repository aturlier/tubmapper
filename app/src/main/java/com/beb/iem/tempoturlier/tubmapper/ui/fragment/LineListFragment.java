package com.beb.iem.tempoturlier.tubmapper.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beb.iem.tempoturlier.tubmapper.R;
import com.beb.iem.tempoturlier.tubmapper.data.model.LineModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;
import com.beb.iem.tempoturlier.tubmapper.ui.activity.MainActivity;
import com.beb.iem.tempoturlier.tubmapper.ui.adapter.BusAdapter;
import com.beb.iem.tempoturlier.tubmapper.ui.presenter.LineListPresenter;
import com.beb.iem.tempoturlier.tubmapper.ui.view.LineListView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class LineListFragment extends Fragment implements LineListView {

    @BindView(R.id.lineList) RecyclerView lineList;

    private LineListPresenter presenter;

    public static LineListFragment newInstance() {
        LineListFragment fragment = new LineListFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bus_list, container, false);
        ButterKnife.bind(this, rootView);

        presenter = new LineListPresenter(this);
        presenter.initBusList();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        lineList.setLayoutManager(layoutManager);
        BusAdapter ba = new BusAdapter(getContext(), ((MainActivity)this.getActivity()).getMapFragment());
        lineList.setAdapter(ba);

        return rootView;
    }

    @Override
    public void initList(HashMap<LineModel, List<StopModel>> linesAndStops) {
        ((BusAdapter) lineList.getAdapter()).setList(linesAndStops);
    }
}
