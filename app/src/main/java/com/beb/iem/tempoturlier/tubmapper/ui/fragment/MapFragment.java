package com.beb.iem.tempoturlier.tubmapper.ui.fragment;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beb.iem.tempoturlier.tubmapper.R;
import com.beb.iem.tempoturlier.tubmapper.TMApplication;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;
import com.beb.iem.tempoturlier.tubmapper.ui.presenter.MapFragmentPresenter;
import com.beb.iem.tempoturlier.tubmapper.ui.utils.map.StopClusterItem;
import com.beb.iem.tempoturlier.tubmapper.ui.utils.map.StopClusterRenderer;
import com.beb.iem.tempoturlier.tubmapper.ui.utils.map.StopMapClusterItemInfoWindowAdapter;
import com.beb.iem.tempoturlier.tubmapper.ui.view.MapFragmentView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.kml.KmlLayer;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by romaintempo on 02/11/2016.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback, MapFragmentView {

    public static final int PERMISSIONS_REQUEST_FINE_LOCATION = 500;

    @BindView(R.id.map_view) MapView mapView;
    private MapFragmentPresenter presenter;
    private GoogleMap gMap;
    private CameraPosition cameraPosition;
    private LayoutInflater inflater;
    private HashMap<String, KmlLayer> kmlList;
    private List<StopClusterItem> stopClusterItems;

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);

        this.inflater = inflater;

        if (mapView != null) {
            mapView.onCreate(savedInstanceState);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        presenter = new MapFragmentPresenter(this);
        kmlList = new HashMap<>();
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.gMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            MapsInitializer.initialize(getContext());
            Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), true));
            if(location != null) {
                gMap.setMyLocationEnabled(true);
                cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                        .zoom(12)                    // Sets the orientation of the camera to east
                        .build();                   // Creates a CameraPosition from the builder
                gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            } else {
                gMap.setMyLocationEnabled(true);
                cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(46.200000, 5.216667))      // Sets the center of the map to location user
                        .zoom(12)                    // Sets the orientation of the camera to east
                        .build();                   // Creates a CameraPosition from the builder
                gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }

        presenter.initialize();

    }

    public void addStopMarkersToMap(List<StopClusterItem> stopClusterItemList) {

        stopClusterItems = stopClusterItemList;

        final ClusterManager<StopClusterItem> clusterManager = new ClusterManager<>(this.getContext(), gMap);
        final StopMapClusterItemInfoWindowAdapter clusterAdapter = new StopMapClusterItemInfoWindowAdapter(this.inflater);
        final StopClusterRenderer clusterRenderer = new StopClusterRenderer(this.getContext(), gMap, clusterManager);

        clusterManager.setRenderer(clusterRenderer);
        clusterManager.getMarkerCollection().setOnInfoWindowAdapter(clusterAdapter);
        clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<StopClusterItem>() {
            @Override
            public boolean onClusterItemClick(StopClusterItem stopMapClusterItem) {
                clusterAdapter.setCurrentClusterItem(stopMapClusterItem);
                return false;
            }
        });
        clusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<StopClusterItem>() {
            @Override
            public boolean onClusterClick(Cluster<StopClusterItem> cluster) {
                String title = "";
                List<String> linesForStop = new ArrayList<>();
                Collection<StopClusterItem> clusters = cluster.getItems();
                for (StopClusterItem clusterX : clusters){
                    title += clusterX.getClusterTitle() + "\n";
                    if(cluster.getSize() < 2) {
                        linesForStop.addAll(clusterX.getLineNumberList());
                    }
                }
                StopClusterItem stopMapClusterItem =  new StopClusterItem(title, linesForStop, cluster.getPosition());
                clusterAdapter.setCurrentClusterItem(stopMapClusterItem);
                return false;
            }
        });

        clusterManager.addItems(stopClusterItemList);

        gMap.setInfoWindowAdapter(clusterAdapter);
        gMap.setOnCameraIdleListener(clusterManager);
        gMap.setOnMarkerClickListener(clusterManager);

    }

    @Override
    public void addKmlLineToMap(InputStream kmlInputStream, String id) {
        if(kmlInputStream != null) {
            try {
                KmlLayer layer = new KmlLayer(gMap, kmlInputStream, getContext());
                layer.addLayerToMap();
                kmlInputStream.close();
                kmlList.put(id, layer);
            } catch (Exception e) {
                Log.e("KML LAYER ERROR", Log.getStackTraceString(e));
            }
        }
    }


    public void highlightLayerOnMap(String id) {
        gMap.clear();
        try {
            kmlList.get(id).addLayerToMap();
        } catch (Exception e) {
            Log.e("KML LAYER ERROR", Log.getStackTraceString(e));
        }
        List<StopModel> stopModels = TMApplication.app().getDataRepository().getStopListForLine(TMApplication.app().getDataRepository().getLineForId(id));
        List<StopClusterItem> clusterItemList = new ArrayList<>();
        for (StopModel stopModel : stopModels) {
            clusterItemList.add(new StopClusterItem(
                    stopModel.getLabel(),
                    null,
                    new LatLng(stopModel.getLatitude(), stopModel.getLongitude())
            ));
        }
        addStopMarkersToMap(clusterItemList);
    }

    public void resetHighlighting() {
        gMap.clear();
        presenter.initializeMap();
    }

    private KmlLayer getLayerFromLineId(String id) {
        return kmlList.get(id);
    }

    @Override
    public void removeKmlFromMap(KmlLayer layer) {
        layer.removeLayerFromMap();
    }

}
