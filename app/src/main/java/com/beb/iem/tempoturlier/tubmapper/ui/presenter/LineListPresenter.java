package com.beb.iem.tempoturlier.tubmapper.ui.presenter;

import com.beb.iem.tempoturlier.tubmapper.TMApplication;
import com.beb.iem.tempoturlier.tubmapper.data.model.LineModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;
import com.beb.iem.tempoturlier.tubmapper.data.repository.DataRepository;
import com.beb.iem.tempoturlier.tubmapper.ui.view.LineListView;

import java.util.HashMap;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by romaintempo on 04/11/2016.
 */

public class LineListPresenter {

    private LineListView view;
    private DataRepository dataRepository;

    public LineListPresenter(LineListView view) {
        this.view = view;
        dataRepository = TMApplication.app().getDataRepository();
    }

    public void initBusList() {
        dataRepository.getAllStopsAndStopGroupsCall()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onCompleted() {}
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext(Boolean aBoolean) {
                        HashMap<LineModel, List<StopModel>> linesAndStops = new HashMap<>();
                        for(LineModel lineModel : dataRepository.getAllLines()) {
                            linesAndStops.put(lineModel, dataRepository.getStopListForLine(lineModel));
                        }
                        view.initList(linesAndStops);
                    }
                });
    }
}
