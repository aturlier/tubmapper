package com.beb.iem.tempoturlier.tubmapper.ui.presenter;

import com.beb.iem.tempoturlier.tubmapper.TMApplication;
import com.beb.iem.tempoturlier.tubmapper.data.model.LineModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;
import com.beb.iem.tempoturlier.tubmapper.data.repository.DataRepository;
import com.beb.iem.tempoturlier.tubmapper.ui.utils.map.StopClusterItem;
import com.beb.iem.tempoturlier.tubmapper.ui.view.MapFragmentView;
import com.google.android.gms.maps.model.LatLng;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by aturlier on 08/11/2016.
 */

public class MapFragmentPresenter {

    private final MapFragmentView view;
    private DataRepository dataRepository;

    public MapFragmentPresenter(final MapFragmentView view) {
        this.view = view;
    }

    public void initialize() {
        dataRepository = TMApplication.app().getDataRepository();

        dataRepository.getAllStopsAndStopGroupsCall()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Boolean>() {
                @Override
                public void onCompleted() {}
                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                }
                @Override
                public void onNext(Boolean success) {
                    if(success) {
                        initializeMap();
                    }
                }
            });
    }

    private void addLinesKMLToMap() {
        for(LineModel lineModel : dataRepository.getAllLines()) {
            addKMLLineToMap(lineModel.getId());
        }
    }

    private void addKMLLineToMap(final String id) {
        dataRepository.getKmlById(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<InputStream>() {
                    @Override
                    public void onCompleted() {}
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext(InputStream inputStream) {
                        view.addKmlLineToMap(inputStream, id);
                    }
                });
    }

    public void initializeMap() {
        List<StopModel> stopList = dataRepository.getAllStops();
        List<StopClusterItem> clusterItemList = new ArrayList<>();
        for (StopModel stopModel : stopList) {
            List<LineModel> lineModels = dataRepository.getLineListForStop(stopModel);
            Set<String> linesForStopSet = new HashSet<>();
            for(LineModel lineModel : lineModels) {
                linesForStopSet.add(lineModel.getLabel().split(" : ")[0]);
            }
            List<String> linesForStopStrings = new ArrayList<>(linesForStopSet);

            if(!linesForStopStrings.isEmpty()) {
                clusterItemList.add(new StopClusterItem(
                        stopModel.getLabel(),
                        linesForStopStrings,
                        new LatLng(stopModel.getLatitude(), stopModel.getLongitude())
                ));
            }
        }

        view.addStopMarkersToMap(clusterItemList);

        addLinesKMLToMap();
    }

}
