package com.beb.iem.tempoturlier.tubmapper.ui.utils.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.util.List;

/**
 * Created by aturlier on 07/11/2016.
 */

public class StopClusterItem implements ClusterItem {

    private String clusterTitle;
    private List<String> lineNumberList;
    private LatLng clusterPosition;
    private boolean clustered = false;

    public StopClusterItem(String clusterTitle, List<String> lineNumberList, LatLng clusterPosition) {
        this.clusterTitle = clusterTitle;
        this.lineNumberList = lineNumberList;
        this.clusterPosition = clusterPosition;
    }

    @Override
    public LatLng getPosition() {
        return clusterPosition;
    }

    public String getClusterTitle() {
        return clusterTitle;
    }

    public List<String> getLineNumberList() {
        return lineNumberList;
    }

    public void setClusterTitle(String clusterTitle) {
        this.clusterTitle = clusterTitle;
    }

    public LatLng getClusterPosition() {
        return clusterPosition;
    }

    public void setClusterPosition(LatLng clusterPosition) {
        this.clusterPosition = clusterPosition;
    }

    public boolean isClustered() {
        return clustered;
    }

    public void setClustered(boolean clustered) {
        this.clustered = clustered;
    }
}
