package com.beb.iem.tempoturlier.tubmapper.ui.utils.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.beb.iem.tempoturlier.tubmapper.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

/**
 * Created by aturlier on 07/11/2016.
 */

public class StopClusterRenderer extends DefaultClusterRenderer<StopClusterItem> {

    private final static int CLUSTER_MINIMAL_VALUE = 6;
    private Context context;
    private IconGenerator iconGenerator;

    public StopClusterRenderer(Context context, GoogleMap map, ClusterManager<StopClusterItem> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
        this.iconGenerator = new IconGenerator(context);
    }

    @Override
    protected void onBeforeClusterItemRendered(StopClusterItem item, MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions);

        final Drawable clusterIcon = context.getDrawable(R.drawable.ic_stop);
        clusterIcon.setTint(ContextCompat.getColor(context, R.color.colorPrimary));
        iconGenerator.setBackground(clusterIcon);
        Bitmap icon = iconGenerator.makeIcon();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<StopClusterItem> cluster, MarkerOptions markerOptions) {
        super.onBeforeClusterRendered(cluster, markerOptions);

        final Drawable clusterIcon = context.getDrawable(R.drawable.round_shape);
        clusterIcon.setTint(ContextCompat.getColor(context, R.color.colorPrimary));
        iconGenerator.setBackground(clusterIcon);
        Bitmap icon = iconGenerator.makeIcon();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster cluster) {
        return cluster.getSize() > CLUSTER_MINIMAL_VALUE;
    }
}
