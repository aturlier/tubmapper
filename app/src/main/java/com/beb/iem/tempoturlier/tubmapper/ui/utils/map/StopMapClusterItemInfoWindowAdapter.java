package com.beb.iem.tempoturlier.tubmapper.ui.utils.map;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.beb.iem.tempoturlier.tubmapper.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by aturlier on 08/11/2016.
 */

public class StopMapClusterItemInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final View view;

    private ClusterItem currentClusterItem;

    @BindView(R.id.item_map_window_stop_info_title) TextView tvTitle;
    @BindView(R.id.item_map_window_stop_info_lines) TextView tvLines;

    public StopMapClusterItemInfoWindowAdapter(LayoutInflater layoutInflater) {
        view = layoutInflater.inflate(R.layout.item_map_window_stop_info, null);
        ButterKnife.bind(this, view);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        if (currentClusterItem instanceof StopClusterItem) {
            StopClusterItem stopClusterItem = (StopClusterItem) currentClusterItem;
            tvTitle.setText(stopClusterItem.getClusterTitle());
            if(!((StopClusterItem) currentClusterItem).isClustered()) {
                String lines = "";
                List<String> lineLabels = ((StopClusterItem) currentClusterItem).getLineNumberList();
                if(lineLabels != null) {
                    for (String lineLabel : lineLabels) {
                        lines += lineLabel + "\n";
                    }
                    tvLines.setText(lines);
                    tvLines.setVisibility(View.VISIBLE);
                }
            }
        }
        return view;
    }

    public void setCurrentClusterItem(ClusterItem currentClusterItem) {
        this.currentClusterItem = currentClusterItem;
    }

}
