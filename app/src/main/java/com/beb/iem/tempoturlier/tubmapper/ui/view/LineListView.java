package com.beb.iem.tempoturlier.tubmapper.ui.view;

import com.beb.iem.tempoturlier.tubmapper.data.model.LineModel;
import com.beb.iem.tempoturlier.tubmapper.data.model.StopModel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by romaintempo on 04/11/2016.
 */

public interface LineListView {
    void initList(HashMap<LineModel, List<StopModel>> linesAndStops);
}
