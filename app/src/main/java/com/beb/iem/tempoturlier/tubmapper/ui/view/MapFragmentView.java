package com.beb.iem.tempoturlier.tubmapper.ui.view;

import com.beb.iem.tempoturlier.tubmapper.ui.utils.map.StopClusterItem;
import com.google.maps.android.kml.KmlLayer;

import java.io.InputStream;
import java.util.List;

/**
 * Created by romaintempo on 08/11/2016.
 */

public interface MapFragmentView {
    void addStopMarkersToMap(List<StopClusterItem> list);
    void addKmlLineToMap(InputStream kmlInputStream, String id);
    void removeKmlFromMap(KmlLayer layer);
}
